from django.contrib import admin
from .models import Article, Comment

admin.site.register(Article) # добавляем модель в админку
admin.site.register(Comment)
