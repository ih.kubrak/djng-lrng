import datetime
from django.db import models
from django.utils import timezone

class Article(models.Model): # конвенция именования в единственном числе
    article_title = models.CharField('название статьи', max_length = 200) # CharField требует обязательно второй аргумент
    article_text = models.TextField('текст статьи')
    pub_date = models.DateTimeField('дата публикации')

    def __str__(self): # для удобного отображения названия
        return self.article_title
    
    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days = 7))
    
    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete = models.CASCADE) # связывает комменты со статьями; второй параметр включает удаление вместе со ст. все комменты
    author_name = models.CharField('имя автора', max_length = 50)
    comment_text = models.CharField('имя автора', max_length = 2500)

    def __str__(self):
        return self.author_name

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'

