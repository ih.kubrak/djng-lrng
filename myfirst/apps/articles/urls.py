from django.urls import path

from . import views # импорт из текущего пакета, где мы находимся

app_name = "articles" # это для правильной урл привязки; чтоб "detail" тянулись только отсюда

urlpatterns = [
    path('', views.index, name = "_index_"),
    # path([route], [view_resolver], name - ?) 
    # урл "acticles/пустая_строка",
    # переход по которому вызовет внутри файла views функцию под названием index.
    # сейчас функция index в моём views.py возвращает просто "Привет, мир!"
    path('<int:article_id>/', views.detail, name = "detail"), # здесь динамически приходит article id и рендерится соотв. статья в отдельной странице
    path('<int:article_id>/leave_comment', views.leave_comment, name = "leave_comment"), # путь для добавления комментариев

] 
