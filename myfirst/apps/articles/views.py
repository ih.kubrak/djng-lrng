from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from articles.models import Article # вместо from .models написал articles.models, потому что не импортировало; видимо, это связано с созданием папки apps

def index(request):
    latest_articles_list = Article.objects.order_by('-pub_date')[:5] #
    return render(request, 'articles/list.html', {'latest_articles_list': latest_articles_list})

def detail(request, article_id):
    try:
        article = Article.objects.get(id = article_id)
    except:
        raise Http404("Статья не найдена")

    latest_comments_list = article.comment_set.order_by('-id')[:10]
    return render(request, 'articles/detail.html', {'article': article, 'latest_comments_list': latest_comments_list})

def leave_comment(request, article_id):
    try:
        article = Article.objects.get(id = article_id)
    except:
        raise Http404("Статья не найдена")

    article.comment_set.create(author_name = request.POST['name'], comment_text = request.POST['text'])

    return HttpResponseRedirect( reverse('articles:detail', args = (article.id, )) )
