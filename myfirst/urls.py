from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('grappelli/', include('grappelli.urls')), # для Grappelli
    path('', include('articles.urls')), # т.е., include([app_name].[routing_file])
    # открывает локальную привязку нашего приложения, и уже из apps/urls.py досматривает путь
    path('admin/', admin.site.urls),
]
